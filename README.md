addressbook
===========

A Symfony project created on July 21, 2019, 5:13 pm.

Prerequirements:

Symfony 3.4
PHP 7 >
Apache

config.yml::

# Doctrine Configuration
doctrine:
    dbal:
        default_connection: default
        driver: pdo_sqlite
        path: '%kernel.project_dir%/app/sqlite.db'
        charset: UTF8

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true


helpful and sequence of commands:

php bin/console server:run

php bin/cons doctrine:database:create

doctrine:generate:entities

doctrine:generate:entity

php bin/console doctrine:schema:update

php bin/console doctrine:schema:update --dump-sql --force

php bin/console doctrine:generate:form AppBundle:AddressBook

php bin/console doctrine:generate:crud AppBundle:AddressBook

